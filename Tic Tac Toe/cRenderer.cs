﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tic_Tac_Toe
{
    static class cRenderer
    {
        public static void DrawMap(cGame game)
        {
            Console.Clear();

            const int xOffset = 3;
            const int yOffset = 2;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.SetCursorPosition(j + xOffset, i + yOffset);
                    Console.Write(game.map[i,j]);
                }
            }
        }

        public static void DrawText(cPlayer player1, cPlayer player2, cGame game)
        {
            Console.SetCursorPosition(10, 2);
            Console.WriteLine(game.GetCurrPlayerToMove(player1, player2, game).name + "'s move.");
            Console.SetCursorPosition(10, 3);
            Console.WriteLine(player1.name + ": " + player1.Score);
            Console.SetCursorPosition(10, 4);
            Console.WriteLine(player2.name + ": " + player2.Score);

            Console.SetCursorPosition(7, 6);
            Console.WriteLine("Use NUM1-9 to place the symbol.");
            Console.SetCursorPosition(7, 7);
            Console.WriteLine("ESC to exit");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tic_Tac_Toe
{
    class cGame
    {
        public bool gameRunning;
        public char neutralChar; // The char that fills the mp by default
        public char[,] map = new char[3, 3]; // Game field
        public int gameTurn;

        public void MapReset() // Use this to reset the map, after game starts or if player chooses to replay
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    map[i,j] = neutralChar;
                }
            }
        }

        public cPlayer GetCurrPlayerToMove(cPlayer player1, cPlayer player2, cGame game) // A method to simplify the code in Main
        {
            if (game.gameTurn % 2 == 0) return player1; else return player2; // Note that class are reference objects, so we dont need to refer to it as to a ref 
        }

        public void Victory(cPlayer player, cGame game)
        {
            Console.Clear();
            Console.SetCursorPosition(10,10);
            Console.WriteLine("Player " + player.name + " is victorious! Press Enter to continue.");
            Console.ReadLine(); Console.Clear();
            MapReset();
            player.Score++;
            game.gameTurn = 0;
        }

        public void CheckEndGame(cPlayer player1, cPlayer player2, cGame game) // Check if the player who just had a move won. Had to put this in Controlls method
        {
            if (CheckWinner(GetCurrPlayerToMove(player1, player2, game)))
            {
                Victory(GetCurrPlayerToMove(player1, player2, game), game);
            }
        }

        public void Controllers(cPlayer player1, cPlayer player2, cGame game)
        {
            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.NumPad1:
                    if (game.map[2, 0] == game.neutralChar) { game.map[2, 0] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;
                case ConsoleKey.NumPad2:
                    if (game.map[2, 1] == game.neutralChar) { game.map[2, 1] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;
                case ConsoleKey.NumPad3:
                    if (game.map[2, 2] == game.neutralChar) { game.map[2, 2] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;
                case ConsoleKey.NumPad4:
                    if (game.map[1, 0] == game.neutralChar) { game.map[1, 0] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;
                case ConsoleKey.NumPad5:
                    if (game.map[1, 1] == game.neutralChar) { game.map[1, 1] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;
                case ConsoleKey.NumPad6:
                    if (game.map[1, 2] == game.neutralChar) { game.map[1, 2] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;
                case ConsoleKey.NumPad7:
                    if (game.map[0, 0] == game.neutralChar) { game.map[0, 0] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;
                case ConsoleKey.NumPad8:
                    if (game.map[0, 1] == game.neutralChar) { game.map[0, 1] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;
                case ConsoleKey.NumPad9:
                    if (game.map[0, 2] == game.neutralChar) { game.map[0, 2] = GetCurrPlayerToMove(player1, player2, game).symbol; CheckEndGame(player1, player2, game); game.gameTurn++; }
                    break;

                case ConsoleKey.Escape:
                    game.gameRunning = false;
                    break;
            }
        }

        public void CheckTruce(cGame game) // if all nodes are filled, restart the game. Had to put this in the Controlls as an IF
        {
            byte nodesLeft = 0;
            for(int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (game.map[i, j] == game.neutralChar) nodesLeft++;
                }
            }
            if (nodesLeft > 0) return;
            Console.Clear();
            Console.SetCursorPosition(10, 10);
            Console.WriteLine("Its a truce! Enter to replay");
            Console.ReadLine(); Console.Clear();
            MapReset();
        }

        public bool CheckWinner(cPlayer player) // The method to check if the passed player filled any rows or cols
        {
            // top row
            if (map[0, 0] == player.symbol && map[0, 1] == player.symbol && map[0, 2] == player.symbol) return true;
            // mid row
            else if (map[1, 0] == player.symbol && map[1, 1] == player.symbol && map[1, 2] == player.symbol) return true;
            // bot row
            else if (map[2, 0] == player.symbol && map[2, 1] == player.symbol && map[2, 2] == player.symbol) return true;
            // left col
            else if (map[0, 0] == player.symbol && map[1, 0] == player.symbol && map[2, 0] == player.symbol) return true;
            // mid col
            else if (map[0, 1] == player.symbol && map[1, 1] == player.symbol && map[2, 1] == player.symbol) return true;
            // right col
            else if (map[0, 2] == player.symbol && map[1, 2] == player.symbol && map[2, 2] == player.symbol) return true;
            // diagonals
            else if (map[0, 0] == player.symbol && map[1, 1] == player.symbol && map[2, 2] == player.symbol) return true;
            else if (map[0, 2] == player.symbol && map[1, 1] == player.symbol && map[2, 0] == player.symbol) return true;

            else return false;
        }

        public cGame()
        {
            gameRunning = true;
            neutralChar = '-';
            gameTurn = 0;
            MapReset();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tic_Tac_Toe
{
    class cPlayer
    {
        public string name;
        public char symbol;
        public byte Score { get; set; }
        
        public cPlayer(string NAME, cGame game)
        {
            Console.Clear();
            Console.WriteLine("Please enter " + NAME + "'s name: ");
            name = Console.ReadLine();

            Console.WriteLine("Please enter " + NAME + "'s symbol: ");
            do
            {
                symbol = Console.ReadKey().KeyChar;
            } while (symbol == game.neutralChar);
        }
    }
}

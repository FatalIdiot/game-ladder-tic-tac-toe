﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tic_Tac_Toe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "TicTacToe";
            Console.CursorVisible = false;

            cGame game = new cGame();
            
            cPlayer player1 = new cPlayer("Player 1", game);
            cPlayer player2 = new cPlayer("Player 2", game);
            
            while(game.gameRunning)
            {
                cRenderer.DrawMap(game);
                cRenderer.DrawText(player1, player2, game);
                game.Controllers(player1, player2, game);
                game.CheckTruce(game);
            }


        }
    }
}
